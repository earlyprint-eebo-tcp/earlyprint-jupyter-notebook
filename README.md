https://earlyprint-eebo-tcp.gitlab.io/earlyprint-jupyter-notebook

A place to build and share notebooks.

## Editing this notebook

When editing the notebook itself, it is constantly re-run and posted to be shared with everyone else.

Before committing to this repo,
```
git config --local include.path ../.gitconfig
```
If it worked, then `git config --includes --list` should show the filter.

You also need to
```
~/path/to/your/env/bin/python -m ipykernel install --name shared-name-of-ipykernel-envs --display-name "Name that ipykernel uses for multiple different envs on different machines"
```
This is because...well, the short version is that Jupyter appears to want to actively defeat any attempt to share ipynb notebooks between people.

You might naively assume that Jupyter will run in whatever conda environment it is invoked from, just like every other Python package that can be invoked as a script.
But as it turns out, Jupyter will immediately turn around and terminate the Python process that invoked it, then trawl through your computer looking for a different Python environment to invoke.
It wants to invoke the Python environment specified in the kernelspec.

If you change the kernelspec to something generic like "python3", as I was doing, that still doesn't mean it will use the Python version it was invoked with.
That just means that secretly, under the hood, it's executing the cells with the root Python, *not* the conda environment it was invoked with.
On some systems the cells will still execute just fine, but on other systems they'll mysteriously fail.
(Well...setting the kernelspec to "python3" *will* make it use the Python that's currently running, if you set up your system *just so*. https://stackoverflow.com/questions/46551200/changing-jupyter-kernelspec-to-point-to-anaconda-python But it depends quite delicately on the host system, and the entire point was portability.)

The only apparent way to make an ipynb notebook portable is to create, in advance, a kernel name to be used by everyone, and put that in the kernelspec.
Thus, if you want to use a different environment to run the notebook, you have to re-run `python -m ipykernel install` from the new environment with the same `--name`.
